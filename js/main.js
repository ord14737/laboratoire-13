// Élément HTML du sélecteur de livre
let selecteur = document.getElementById('selecteur');

// Listes de livres
let livres = [
    {
        isbn: '9782709624930',
        titre: 'Da Vinci Code',
        editeur: 'JCLATTES',
        annee: 2004,
        auteur: 'Dan Brown'
    },
    {
        isbn: '9781781101032',
        titre: 'Harry Potter à L\'école des Sorciers',
        editeur: 'Folio junior',
        annee: 2015,
        auteur: 'J. K. Rowling'
    },
    {
        isbn: '9782266282390',
        titre: 'Le Seigneur des Anneaux: La Fraternité de l\'anneau',
        editeur: 'POCKET',
        annee: 2018,
        auteur: 'J R R Tolkien'
    }
];

// Affichage des livres dans le sélecteur
for(let i = 0 ; i < livres.length ; i++){
    selecteur.innerHTML += 
        '<option value="' + i + '">' + 
            livres[i].titre + 
        '</option>'
}

// Remplir les champs du formulaire lorsqu'on clique
// sur un des élément du sélecteur
selecteur.onchange = function(){
    let selection = livres[selecteur.value];

    let inputISBN = document.querySelector('#isbn input');
    inputISBN.value = selection.isbn;

    let inputTitre = document.querySelector('#titre input');
    inputTitre.value = selection.titre;

    let inputEditeur = document.querySelector('#editeur input');
    inputEditeur.value = selection.editeur;

    let inputAnnee = document.querySelector('#annee input');
    inputAnnee.value = selection.annee;

    let inputAuteur = document.querySelector('#auteur input');
    inputAuteur.value = selection.auteur;
}

// Évènement pour ajouter un livre
// (À compléter ci-dessous ...)


// Évènement pour modifier un livre
// (À compléter ci-dessous ...)


// Évènement pour supprimer un livre
// (À compléter ci-dessous ...)
